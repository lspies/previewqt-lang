<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>PQAbout</name>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="35"/>
        <source>About</source>
        <extracomment>window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="102"/>
        <source>Click here to show configuration of this build</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="127"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="132"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="137"/>
        <source>Website:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="142"/>
        <source>Developer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="147"/>
        <source>Contact:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="216"/>
        <source>PreviewQt is a lightweight application allowing for quickly previewing a wide range of files, from images and videos to documents and archives. A list of supported file formats can be found on its website, though the number of actually supported formats might be even higher. If you like PreviewQt and would like to have a fully featured image viewer along the same lines check out PhotoQt: https://photoqt.org</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="270"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="276"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQAbout.qml" line="331"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQArchive</name>
    <message>
        <location filename="../qml/components/imageitems/PQArchive.qml" line="345"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCFileFormats</name>
    <message>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="64"/>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="77"/>
        <source>ERROR getting default image formats</source>
        <extracomment>This is the window title of an error message box</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="65"/>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="78"/>
        <source>Not even a read-only version of the database of default image formats could be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="65"/>
        <location filename="../cplusplus/singletons/pqc_fileformats.cpp" line="78"/>
        <source>Something went terribly wrong somewhere!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQDocument</name>
    <message>
        <location filename="../qml/components/imageitems/PQDocument.qml" line="274"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQEPUB</name>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="302"/>
        <source>Progress in current chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="377"/>
        <source>Go to previous section/chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="407"/>
        <source>Go to next section/chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="427"/>
        <source>Current section/chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="460"/>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQEPUB.qml" line="495"/>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQHelp</name>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="34"/>
        <location filename="../qml/windows/PQHelp.qml" line="73"/>
        <source>Help</source>
        <extracomment>window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="82"/>
        <source>In the main application window, combining the Control modifier with either of O, P, or I launches the action to open a file, the settings, or the About information, respectively. F1 opens this help message, Escape hides the application, and Ctrl+Q quits it altogether. Double clicking on a loaded image toggles the fullscreen mode. Then there exists a customizable shortcut for opening a loaded file in an external application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="90"/>
        <source>In the settings window, Escape, Enter, and Return all close the window, and Ctrl+Tab switches between the two tabs.</source>
        <extracomment>Please keep the html tags for bold text in place as they help with parsing this block of text</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="98"/>
        <source>In both the Help and About window, either one of Escape, Enter, and Return close the window.</source>
        <extracomment>Please keep the html tags for bold text in place as they help with parsing this block of text</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="106"/>
        <source>When special formats like archives and documents are loaded, the arrow keys allow switching between pages/files, and Home/End go to the beginning/end of the loaded document. Similarly, when video files are loaded, the space bar toggles the play status, the arrow keys jump left/right in the video file, and Home/End go to the start and end of the video.</source>
        <extracomment>Please keep the html tags for bold text in place as they help with parsing this block of text</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="118"/>
        <source>Need more help? Then check out the website for PreviewQt:</source>
        <extracomment>Please keep the html tags for bold text in place as they help with parsing this block of text</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQHelp.qml" line="165"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQIconButton</name>
    <message>
        <location filename="../qml/components/PQIconButton.qml" line="87"/>
        <source>Action not supported for this file type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQIconButton.qml" line="87"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQIconButton.qml" line="100"/>
        <source>Trigger action</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQMainWindow</name>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="135"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="140"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="150"/>
        <source>Open externally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="163"/>
        <source>Rotate left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="171"/>
        <source>Rotate right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="179"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="187"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="195"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="240"/>
        <source>Click to open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="383"/>
        <source>PreviewQt launched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="384"/>
        <source>PreviewQt has been launched and hidden to the system tray.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="526"/>
        <source>File does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="526"/>
        <source>The requested file does not exist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="584"/>
        <source>Some of the external applications cannot be started or are not set up correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="593"/>
        <source>Please correct this in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="605"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/PQMainWindow.qml" line="611"/>
        <source>Go to settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQSettings</name>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="38"/>
        <location filename="../qml/windows/PQSettings.qml" line="212"/>
        <location filename="../qml/windows/PQSettings.qml" line="423"/>
        <source>Settings</source>
        <extracomment>Same as tab name but used as title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="155"/>
        <source>General</source>
        <extracomment>Tab name: general settings</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="161"/>
        <source>External applications</source>
        <extracomment>Tab name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="218"/>
        <location filename="../qml/windows/PQSettings.qml" line="429"/>
        <source>Note: Settings will be saved automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="230"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="258"/>
        <source>Keep top bar always visible</source>
        <extracomment>the top bar is the bar with the buttons</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="269"/>
        <source>Launch PreviewQt hidden to system tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="280"/>
        <source>Hide PreviewQt when losing focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="299"/>
        <source>Launch PreviewQt maximized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="312"/>
        <source>Default window size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="370"/>
        <source>Resize window to content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="437"/>
        <source>Shortcut to load in external application:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="361"/>
        <source>Only set window size at launch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="458"/>
        <source>This is a reserved shortcut for PreviewQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="470"/>
        <source>Shortcuts with the Ctrl modifier alone are reserved for PreviewQt.</source>
        <extracomment>The alone here refers to the fact that if the Ctrl modifier key is used without any other modifier keys (Shift, ...), then this is reserved.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="484"/>
        <source>Hide window after launching external application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="496"/>
        <source>External application for images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="545"/>
        <source>External application for documents:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="594"/>
        <source>External application for videos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="643"/>
        <source>External application for archives:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="692"/>
        <source>External application for comic books:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="741"/>
        <source>External application for E-books:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="790"/>
        <source>External application for text documents:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQSettings.qml" line="865"/>
        <source>Close</source>
        <extracomment>written on button</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQTopRow</name>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="80"/>
        <source>Open a file (Ctrl+O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="92"/>
        <source>Open settings (Ctrl+P)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="104"/>
        <source>Open in external application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="129"/>
        <source>Rotate image to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="148"/>
        <source>Rotate image to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="174"/>
        <source>About PreviewQt (Ctrl+I)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="187"/>
        <source>Help (F1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTopRow.qml" line="200"/>
        <source>Quit PreviewQt (Ctrl+Q)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQTrayIcon</name>
    <message>
        <location filename="../qml/components/PQTrayIcon.qml" line="48"/>
        <source>Hide window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTrayIcon.qml" line="48"/>
        <source>Show window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PQTrayIcon.qml" line="54"/>
        <source>Quit PreviewQt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQTxt</name>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="182"/>
        <source>Copy selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="189"/>
        <source>Search for selected text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="198"/>
        <source>Select all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="204"/>
        <source>Copy all content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="314"/>
        <source>search in file (Ctrl+F)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="327"/>
        <source>Search in file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="584"/>
        <source>process file (Ctrl+R)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="597"/>
        <source>Process text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="645"/>
        <source>settings specific for text files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="661"/>
        <source>Show settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="804"/>
        <source>Case-sensitive search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/imageitems/PQTxt.qml" line="832"/>
        <source>Wrap lines</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQWelcome</name>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="34"/>
        <location filename="../qml/windows/PQWelcome.qml" line="74"/>
        <source>Welcome to PreviewQt</source>
        <extracomment>window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="84"/>
        <source>PreviewQt is a simple application for previewing all kinds of files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="91"/>
        <source>It is optimized for speed and lightweight and thus does not have all the features one might expect from an image/document viewer. You can always pass on a file to an external application through a button and shortcut. The selection of external applications can be adjusted freely.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="98"/>
        <source>When not in use, PreviewQt sits idle in the system tray, ready to be shown as instanteneously as possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="106"/>
        <source>Questions? Feedback? Send me an email:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="113"/>
        <source>Also check out the website:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/windows/PQWelcome.qml" line="142"/>
        <source>Get started</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>imageprovider</name>
    <message>
        <location filename="../cplusplus/files/pqc_providerfull.cpp" line="54"/>
        <source>File failed to load, it does not exist!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
